#!/usr/bin/python3 -u
#coding=utf8

#######################################################################################

import sys
import os
import struct
from datetime import datetime
from ctypes import (CDLL, get_errno)
from ctypes.util import find_library
from socket import (socket, AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI, SOL_HCI, HCI_FILTER)
import rrdtool

#######################################################################################

C_PACKET_SIZE = 29
C_BUFFER_SIZE = 2048

#######################################################################################

class TBleDevice:

    sock = None
    bluez = None
    data = None

    # noinspection PyUnresolvedReferences
    def enable ( self, _enable ):
        err = self.bluez.hci_le_set_scan_enable (
            self.sock.fileno(),
            _enable,  # 1 - turn on;  0 - turn off
            0,  # 0-filtering disabled, 1-filter out duplicates
            1000  # timeout
        )
        if err < 0:
            errnum = get_errno()
            raise Exception("{} {}".format(err.errorcode[errnum], os.strerror(errnum)))

    def init ( self ):
        # check system rights
        if not os.geteuid() == 0:
            sys.exit ( "script only works as root" )

        # load bluetooth library
        btlib = find_library ( "bluetooth" )
        if not btlib:
            raise Exception ( "Can't find required bluetooth libraries (need to install bluez)" )
        self.bluez = CDLL ( btlib, use_errno = True )

        #get device id
        dev_id = self.bluez.hci_get_route ( None )

        # socket
        self.sock = socket ( AF_BLUETOOTH, SOCK_RAW, BTPROTO_HCI )
        self.sock.bind ( (dev_id,) )

        err = self.bluez.hci_le_set_scan_parameters ( self.sock.fileno(), 0, 0x10, 0x10, 0, 0, 1000 )
        if err < 0:
            raise Exception ( "Set scan parameters failed" )
            # occurs when scanning is still enabled from previous call

        # allows LE advertising events
        hci_filter = struct.pack ( "<IQH", 0x00000010, 0x4000000000000000, 0 )
        self.sock.setsockopt ( SOL_HCI, HCI_FILTER, hci_filter )

    def scan ( self, on_found_clbk ):
        self.enable ( True )
        try:
            while True:
                data = self.sock.recv ( C_BUFFER_SIZE )
                if on_found_clbk: on_found_clbk ( data )
                else: self.print()

        except KeyboardInterrupt:
            print ( "...Interrupted...Stoping BLE scanning..." )
            self.enable ( False )

    def print ( self ):
        if self.data:
            print('[' + ':'.join("{0:02X}".format(x) for x in self.data[12:6:-1]) + '] ({:d} bytes)'.format(len(self.data))),
            print ( ' '.join("{0:02x}".format(x) for x in self.data ) )

    def stop ( self ):
        self.enable ( False )

#######################################################################################

def c ( string, color ):
    return "\033[{}{}\033[0m".format ( color, string )

class TTempHumSensor:
    idnum : int = 0
    name : str = ""
    address : str = ""
    timestamp : str = ""
    temperature : float = 0.0
    humidity : float = 0.0
    voltage : float = 0.0
    disabled : bool = False

    def __init__ ( self, _idnum : int, _name : str , _address : str ):
        self.idnum = _idnum
        self.name = _name
        self.address = _address

    def parse ( self, _data ):
        if self.disabled: return False
        addr = ':'.join("{0:02X}".format(x) for x in _data[12:6:-1])
        if ( addr == self.address ) and ( len(_data) > 29 ):
            self.timestamp   = datetime.now().strftime ( "%Y%m%d%H%M%S" )
            self.temperature = ( _data[24] + 256 * _data[25] ) / 100.0
            self.humidity    = ( _data[26] + 256 * _data[27] ) / 100.0
            self.voltage     = ( _data[28] + 256 * _data[29] ) / 1000.0
            return True
        return False

    def printx ( self ):
        q = self.idnum + 30
        c0 = "\033[0;{};40m".format ( q )
        c1 = "\033[1;{};40m".format ( q )
        c2 = "\033[2;{};40m".format ( q )
        cd = "\033[2;37;40m"
        cx = "\033[0m"
        print ( ( cd + "{:13.13s} " + c0 + " {:9.9s}" + cx ).format ( self.timestamp, self.name ) +
                ( cd + "T=" + c0 + "{:5.2f}°C " + cx ).format ( self.temperature ) +
                ( cd + "H=" + c0 + "{:2.2f}% " + cx ).format ( self.humidity ) +
                ( cd + "U=" + c0 + "{:2.2f}V ({:2.2f}%)" + cx ).format ( self.voltage, self.voltage / 0.033 ) )

#######################################################################################

def getAddress ( _data ):
    return ':'.join("{0:02X}".format(ord(x)) for x in _data[12:6:-1])

def getTemperature ( _data ):
    return ( ord(_data[24]) + 256 * ord(_data[25]) ) / 100.0

def getHumidity ( _data ):
    return ( ord(_data[26]) + 256 * ord(_data[27]) ) / 100.0

def getBatteryVoltage ( _data ):
    return ( ord(_data[26]) + 256 * ord(_data[27]) ) / 100.0

def printData ( _data ):
    print('[' + ':'.join("{0:02X}".format(x) for x in _data[12:6:-1]) + '] ({:d} bytes)'.format(len(_data))),
    print(' '.join("{0:02X}".format(x) for x in _data))

#######################################################################################

sensors = {
    TTempHumSensor(1, "Дневна",  "A4:C1:38:D9:60:C6"),
    TTempHumSensor(2, "Спалня",  "A4:C1:38:9F:60:BE"),
    TTempHumSensor(3, "Коридор", "A4:C1:38:23:A1:1B"),
    TTempHumSensor(4, "Валери",  "A4:C1:38:CD:28:A6"),
    TTempHumSensor(5, "Детска",  "A4:C1:38:35:22:B1"),
    TTempHumSensor(6, "Таван",   "A4:C1:38:8F:3B:1B"),
}

dev : TBleDevice = TBleDevice()

#######################################################################################

def found_clbk ( data ):
    for s in sensors:
        if s.parse ( data ):
            s.printx()

dev.init()
dev.scan ( found_clbk )

#######################################################################################

        # print bluetooth address from LE Advert. packet
        #print(':'.join("{0:02x}".format(x) for x in data[12:6:-1]))
        #print(':'.join("{0:02X}".format(ord(x)) for x in data[12:6:-1]))
        #print('-'.join("{0:02X}".format(ord(x)) for x in data[13:2:1]))
        #print(data)

        #if ord(data[7]) == 190 and len(data) >= 32:
        #    print(' >>> '),
        #    temp = ( ord(data[24]) + 256 * ord(data[25]) ) / 100.0
        #    hum = ( ord(data[2

        #    6]) + 256 * ord(data[27]) ) / 100.0
        #    print ( "T={:2.2f}°C H={:2.2f}%".format ( temp, hum ) )
        #else:
        #    print('')

# if __name__ == "__main__":
#     try:
#         main()
#     except KeyboardInterrupt:
#         print ( "...Interrupted...Stoping BLE scanning..." )
#         enableBle ( False )

# =====  ==============  =================  =====  =====  ==  =================  =====  ============  =====  =====
# ???    ???             MAC                ?      ?      ?   MAC                T      H      B mV           ??
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
# 00 01  02 03 04 05 06  07 08 09 10 11 12  13 14  15 16  17  18 19 20 21 22 23  24 25  26 27  28 29  30 31  32 33
#                                            0  1   2  3   4   5  6  7  9 10 11  12 13  14 15  16 17  18 19  20 21
#         0  1  2  3  4   5  6  7  8  9 10  11 12  13 14  15  16 17 18 19 20 21  22 23  24 25  26 27  28 29  30 31
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
# 04 3E  1F 02 01 00 00  1B A1 23 38 C1 A4  13 12  16 1A  18  1B A1 23 38 C1 A4  B5 06  E1 18  B1 0B  58 44  0D A9
# 04 3E  1F 02 01 00 00  BE 60 9F 38 C1 A4  13 12  16 1A  18  BE 60 9F 38 C1 A4  DE 07  06 18  D8 0B  5C CA  0D BC
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
# 04 3E  1F 02 01 00 00  B1 22 35 38 C1 A4  13 12  16 1A  18  B1 22 35 38 C1 A4  46 07  1F 19  37 0B  4A E3  0D A9
# [20210126190252] Детска         : T=18.62°C H=64.31% U=2.87V
# 04 3E  1F 02 01 00 00  B1 22 35 38 C1 A4  13 12  16 1A  18  B1 22 35 38 C1 A4  46 07  2B 19  35 0B  4A E4  0D A9
# [20210126190302] Детска         : T=18.62°C H=64.43% U=2.87V
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
# 04 3E  1F 02 01 00 00  1B 3B 8F 38 C1 A4  13 12  16 1A  18  1B 3B 8F 38 C1 A4  C6 02  66 1A  28 0B  48 80  0D B2
# [20210126190254] Таван          : T=7.10°C H=67.58% U=2.86V
# 04 3E  1F 02 01 00 00  1B 3B 8F 38 C1 A4  13 12  16 1A  18  1B 3B 8F 38 C1 A4  C6 02  66 1A  28 0B  48 80  0D AE
# [20210126190304] Таван          : T=7.10°C H=67.58% U=2.86V
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
# 04 3E  1F 02 01 00 00  A6 28 CD 38 C1 A4  13 12  16 1A  18  A6 28 CD 38 C1 A4  19 07  AA 19  B8 0B  58 FC  0D AA
# [20210126190255] Валери         : T=18.17°C H=65.70% U=3.00V
# 04 3E  1F 02 01 00 00  A6 28 CD 38 C1 A4  13 12  16 1A  18  A6 28 CD 38 C1 A4  1A 07  B0 19  B9 0B  59 FD  0D AA
# [20210126190305] Валери         : T=18.18°C H=65.76% U=3.00V
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
# 04 3E  1F 02 01 00 00  C6 60 D9 38 C1 A4  13 12  16 1A  18  C6 60 D9 38 C1 A4  26 08  48 17  CC 0B  5B A4  0D AC
# [20210126190258] Дневна         : T=20.86°C H=59.60% U=3.02V
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
# 04 3E  1F 02 01 00 00  BE 60 9F 38 C1 A4  13 12  16 1A  18  BE 60 9F 38 C1 A4  EF 07  1A 18  EB 0B  5E 16  0D C0
# [20210126190300] Спалня         : T=20.31°C H=61.70% U=3.05V
# =====  ==============  =================  =====  =====  ==  =================  =====  =====  =====  =====  =====
